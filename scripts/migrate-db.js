const path = require('path')
const envPath = path.resolve(process.cwd(), '.env.local')

console.log({ envPath })

require('dotenv').config({ path: envPath })

const mysql = require('serverless-mysql')

const db = mysql({
  config: {
    host: process.env.MYSQL_HOST,
    database: process.env.MYSQL_DATABASE,
    user: process.env.MYSQL_USERNAME,
    password: process.env.MYSQL_PASSWORD,
    port: process.env.MYSQL_PORT,
  },
})

async function query(q) {
  try {
    const results = await db.query(q)
    await db.end()
    return results
  } catch (e) {
    throw Error(e.message)
  }
}

// Drop project tables if they exist and recreate
async function migrate() {
  try {
    await query(`SET SESSION sql_mode = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION,ANSI_QUOTES'`)

    await query(`DROP TABLE IF EXISTS LineItems`)
    await query(`DROP TABLE IF EXISTS Orders`)
    await query(`DROP TABLE IF EXISTS Customers`)
    await query(`DROP TABLE IF EXISTS Couriers`)
    await query(`DROP TABLE IF EXISTS MenuItems`)
    await query(`DROP TABLE IF EXISTS RestaurantLocations`)
    await query(`DROP TABLE IF EXISTS RestaurantBrands`)
    console.log('Tables cleared')
    
    await query(`CREATE TABLE RestaurantBrands (
        brandId int NOT NULL AUTO_INCREMENT UNIQUE,
        name varchar(50) NOT NULL,
        logo varchar(100), 
        PRIMARY KEY(brandId)
    )`)
    console.log('RestaurantBrands table created')
    await query(`CREATE TABLE RestaurantLocations (
        locationId int NOT NULL AUTO_INCREMENT UNIQUE,
        brand int NOT NULL,
        openTime TIME NOT NULL,
        closeTime TIME NOT NULL,
        address varchar(50) NOT NULL,
        PRIMARY KEY(locationId),
        CONSTRAINT brand_fk FOREIGN KEY(brand) REFERENCES RestaurantBrands (brandId)
    )`)
    console.log('RestaurantLocations table created')
    await query(`CREATE TABLE MenuItems (
        menuItemId int NOT NULL AUTO_INCREMENT UNIQUE,
        name varchar(150) NOT NULL,
        description varchar(150),
        price decimal(5, 2) NOT NULL,
        location int NOT NULL,
        PRIMARY KEY(menuItemId),
        CONSTRAINT location_fk FOREIGN KEY (location) REFERENCES RestaurantLocations (locationId)
    )`)
    console.log('MenuItems table created')
    await query(`CREATE TABLE Couriers (
        courierId int NOT NULL AUTO_INCREMENT UNIQUE,
        name varchar(50),
        email varchar(50) NOT NULL,
        PRIMARY KEY(courierId)
    )`)
    console.log('Couriers table created')
    await query(`CREATE TABLE Customers (
        customerId int NOT NULL AUTO_INCREMENT UNIQUE,
        name varchar(50),
        email varchar(50) NOT NULL UNIQUE,
        PRIMARY KEY(customerId)
    )`)
    console.log('Customers table created')
    await query(`CREATE TABLE Orders (
        orderId int NOT NULL AUTO_INCREMENT UNIQUE,
        customer int NOT NULL,
        location int NOT NULL,
        courier int, 
        status varchar(50) NOT NULL,
        PRIMARY KEY(orderId),
        CONSTRAINT customer_fk FOREIGN KEY (customer) REFERENCES Customers (customerId),
        CONSTRAINT order_location_fk FOREIGN KEY (location) REFERENCES RestaurantLocations (locationId),
        CONSTRAINT courier_fk FOREIGN KEY (courier) REFERENCES Couriers (courierId)
    )`)
    console.log('Orders table created')
    await query(`CREATE TABLE LineItems (
        lineItemId int NOT NULL AUTO_INCREMENT UNIQUE,
        "order" int NOT NULL,
        quantity int NOT NULL,
        menuItem int NOT NULL,
        PRIMARY KEY(lineItemId),
        CONSTRAINT order_fk FOREIGN KEY ("order") REFERENCES Orders (orderId)
    )`)
    console.log('LineItems table created')
  } catch (e) {
    console.error('Could not run migration, double check your credentials.')
    process.exit(1)
  }
}

migrate().then(() => process.exit())
