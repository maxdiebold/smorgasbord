# Smörgåsbord
Smörgåsbord is a serverless web app for food pickup/delivery.  
The project is intended to offer comprehensive administrative capabilities, i.e. it is not customer-facing.  

<img src="demo.gif" alt="Dashboard demo" width="507" height="320" />  

The primary tools used are:  
- Next.js
- AWS S3 buckets
- MySQL

## Steps to run
Copy the `.env.local.example` to `.env.local` and populate the fields with credentials for a local or remote MySQL database and an AWS S3 bucket.  

Then, run the migration script to create the necessary tables:  
`node scripts/migrate-db.js`  

Finally, if running locally, start the application:  
`yarn dev` / `npm run dev`  

***OR***

`yarn build; yarn start` / `npm run build; npm run start`
